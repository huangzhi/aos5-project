"""use path generation to solve max flow problem"""
import numpy as np
from loguru import logger
from scipy.optimize import linprog


def generate_graph(n=10) -> np.ndarray:
    adj = np.random.randint(0, 100, size=(n, n))
    adj += adj.T  # sym
    adj -= np.diag(np.diag(adj))
    return adj


class Solver:
    def __init__(self, graph) -> None:
        self.graph = graph
        self.n = graph.shape[0]
        self.edges = [
            [j for j in range(self.n) if self.graph[i][j] > 0]
            for i in range(self.n)
        ]
        self.edge_map = {
            (i, j): idx for idx, (i, j) in enumerate(zip(*np.nonzero(graph)))
        }
        self.n_edge = len(self.edge_map)

    def calculate_cost(self, path, edge_weight):
        indices = [
            self.edge_map[path[i], path[i + 1]] for i in range(len(path) - 1)
        ]
        cost = edge_weight[indices].sum()
        return cost

    def dijkstra(self, start, end, edge_weight):
        """calculate shortest distance"""
        dis = np.full(self.n, np.inf)
        vis = np.full(self.n, False)
        dis[start] = 0
        for i in range(0, self.n):
            u = 0
            mind = np.inf
            for j in range(0, self.n):
                if not vis[j] and dis[j] < mind:
                    u = j
                    mind = dis[j]
            vis[u] = True
            if u == end:
                return dis
            for v in self.edges[u]:
                id = self.edge_map[(u, v)]
                weight = edge_weight[id]
                dis[v] = min(dis[u] + weight, dis[v])

    def shortest_path(self, start, end, edge_weight):
        dis = self.dijkstra(start, end, edge_weight)
        path = []
        curr = end  # current node
        vis = np.full(self.n, False)
        while curr != start:
            vis[curr] = True
            path.append(curr)
            prev_node = [
                i
                for i in range(self.n)
                if self.graph[i][curr] > 0 and not vis[i]
            ]
            curr = min(
                prev_node,
                key=lambda x: dis[x] + edge_weight[self.edge_map[(x, curr)]],
            )
        path.append(curr)
        return path[::-1]

    def path2edges(self, path):
        vec = np.zeros(self.n_edge)
        indices = [
            self.edge_map[path[i], path[i + 1]] for i in range(len(path) - 1)
        ]
        vec[indices] = 1
        return vec

    def solve(self, start, end):
        """main algorithm"""
        paths = []
        # get some paths
        for i in range(3):
            edge_weight = np.random.rand(self.n_edge)
            paths.append(self.shortest_path(start, end, edge_weight))

        # edge limits
        b = np.zeros(self.n_edge)
        for k, id in self.edge_map.items():
            b[id] = self.graph[k]

        while True:
            B = np.array([self.path2edges(p) for p in paths]).T
            # solve the dual
            A_ub = -B.T
            b_ub = -np.ones(len(paths))
            c = b
            res_dual = linprog(c, A_ub=A_ub, b_ub=b_ub, method="highs")
            logger.debug(f"current max flow: {res_dual.fun}")
            new_weight = res_dual.x
            new_path = self.shortest_path(start, end, new_weight)
            if self.calculate_cost(new_path, new_weight) < 1:
                logger.debug(f"find {new_path=}")
                paths.append(new_path)
            else:
                break

        c = np.ones(len(paths))
        A_ub = B
        b_ub = b
        res_prime = linprog(-c, A_ub=A_ub, b_ub=b_ub, method="highs")
        logger.info(
            f"Algorithm finished, prime: {- res_prime.fun}, dual: {res_dual.fun}"
        )
        assert np.allclose(
            -res_prime.fun, res_dual.fun
        ), "not find optimal solution"
        return paths, res_prime.x


def main():
    N = 10
    graph = generate_graph(N)
    solver = Solver(graph)
    start, end = 0, N - 1
    paths, flows = solver.solve(start, end)
    for path, flow in zip(paths, flows):
        if flow > 0:
            print(f"{flow=}, {path=}")
    print(f"max flow from {start} to {end} is {flows.sum()}")


if __name__ == "__main__":
    main()
